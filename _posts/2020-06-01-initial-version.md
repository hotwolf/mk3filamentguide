---
layout: post
title:  Initial Version
---

The initial version of the MK3FilamentGuide has been printed, assembled,
and testet. The design has many things that could be improved, but for
now it does its job well.

![The first test run]({{ site.url }}/images/test_run.jpg)
![Close up]({{ site.url }}/images/close_up.jpg)
